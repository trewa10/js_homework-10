const tabsCollection = document.querySelectorAll(".tabs-title");

// Додаємо всім елементам з описом до вкладок клас hidden, окрім дефолтного, який відобразиться 
const liDescCollection = document.querySelector("#tabs-content").children;
for (let li of liDescCollection) {
    li.classList.toggle("hidden");
}

// функція, яка перемикає вкладки, та порівнює значення дата атрибутів вкладки та опису
const tabSwitcher = (e) => {
    tabsCollection.forEach(el => el.classList.remove("active"));
    e.target.classList.add("active");
    for (let li of liDescCollection) {
        if (li.dataset.name === e.target.dataset.name) {
            li.classList.remove("hidden");
        // В даному випадку є спосіб обійтись і без атрибутів data- , але в майбутньому текст може змінитись і цей спосіб уже не працюватиме
        // if (li.innerText.indexOf(e.target.innerText) >= 0) {
        //     li.classList.remove("hidden");
        } else {
            li.classList.add("hidden");
        }
    }
};

// Навішуємо подію на всі таби
tabsCollection.forEach(el => {
el.addEventListener("click", tabSwitcher);    
});











